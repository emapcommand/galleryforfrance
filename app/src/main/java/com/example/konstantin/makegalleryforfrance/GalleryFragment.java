package com.example.konstantin.makegalleryforfrance;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;

public class GalleryFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gallery, null);

        //писать код для галереи здесь. MainActivity не трогайте
        //чтобы заинфлетить вью писать такое: TextView tv = (TextView) view.findViewById(R.id.tv)
        //уловили,да, "view." перед "findViewById" - вот и отличие

        return view;
    }
}
